import { CounterActionTypesEnum } from "./action-types.enum";
import { IAction } from "./actions";

export interface ICounterDoubleAction
  extends IAction<CounterActionTypesEnum.DOUBLE, undefined> {}

export const counterDoubleAction = (): ICounterDoubleAction => ({
  type: CounterActionTypesEnum.DOUBLE,
  payload: undefined,
});
