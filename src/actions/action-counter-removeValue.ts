import { CounterActionTypesEnum } from "./action-types.enum";
import { IAction } from "./actions";

export interface ICounterRemoveValueAction
  extends IAction<typeof CounterActionTypesEnum.REMOVE_VALUE, number> {}

export const counterRemoveValueAction = (value: number) => ({
  type: CounterActionTypesEnum.REMOVE_VALUE,
  payload: value,
});
