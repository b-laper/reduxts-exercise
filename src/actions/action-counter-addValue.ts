import { CounterActionTypesEnum } from "./action-types.enum";
import { IAction } from "./actions";

export interface ICounterAddValueAction
  extends IAction<typeof CounterActionTypesEnum.ADD_VALUE, number> {}

export const counterAddValueAction = (value: number) => ({
  type: CounterActionTypesEnum.ADD_VALUE,
  payload: value,
});
