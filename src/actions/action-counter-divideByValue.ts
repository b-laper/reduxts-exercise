import { CounterActionTypesEnum } from "./action-types.enum";
import { IAction } from "./actions";

export interface ICounterDivideByValueAction
  extends IAction<typeof CounterActionTypesEnum.DIVIDE_BY_VALUE, number> {}

export const counterDivideByValueAction = (
  value: number
): ICounterDivideByValueAction => ({
  type: CounterActionTypesEnum.DIVIDE_BY_VALUE,
  payload: value,
});
