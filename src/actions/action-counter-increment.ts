import { CounterActionTypesEnum } from "./action-types.enum";
import { IAction } from "./actions";

export interface ICounterIncrementAction
  extends IAction<typeof CounterActionTypesEnum.INCREMENT, undefined> {}

export const counterIncrementAction = (): ICounterIncrementAction => ({
  type: CounterActionTypesEnum.INCREMENT,
  payload: undefined,
});
