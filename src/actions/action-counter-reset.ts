import { CounterActionTypesEnum } from "./action-types.enum";
import { IAction } from "./actions";

export interface ICounterResetAction
  extends IAction<typeof CounterActionTypesEnum.RESET, undefined> {}

export const counterResetAction = (): ICounterResetAction => ({
  type: CounterActionTypesEnum.RESET,
  payload: undefined,
});
