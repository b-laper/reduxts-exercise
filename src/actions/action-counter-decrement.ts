import { CounterActionTypesEnum } from "./action-types.enum";
import { IAction } from "./actions";

export interface ICounterDecrementAction
  extends IAction<typeof CounterActionTypesEnum.DECREMENT, undefined> {}

export const counterDecrementAction = (): ICounterDecrementAction => ({
  type: CounterActionTypesEnum.DECREMENT,
  payload: undefined,
});
