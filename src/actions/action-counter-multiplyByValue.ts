import { CounterActionTypesEnum } from "./action-types.enum";
import { IAction } from "./actions";

export interface ICounterMultiplyByValueAction
  extends IAction<typeof CounterActionTypesEnum.MULTIPLY_BY_VALUE, number> {}

export const counterMultiplyByValueAction = (
  value: number
): ICounterMultiplyByValueAction => ({
  type: CounterActionTypesEnum.MULTIPLY_BY_VALUE,
  payload: value,
});
