import React, { useCallback, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { counterAddValueAction } from "./actions/action-counter-addValue";
import { counterDecrementAction } from "./actions/action-counter-decrement";
import { counterDivideByValueAction } from "./actions/action-counter-divideByValue";
import { counterDoubleAction } from "./actions/action-counter-double";
import { counterIncrementAction } from "./actions/action-counter-increment";
import { counterMultiplyByValueAction } from "./actions/action-counter-multiplyByValue";
import { counterRemoveValueAction } from "./actions/action-counter-removeValue";
import { counterResetAction } from "./actions/action-counter-reset";
import "./App.css";
import { RootStoreType } from "./store/store";

function App() {
  const [inputValue, setInputValue] = useState<number>(0);
  const [error, setError] = useState<boolean>(false);
  const dispatch = useDispatch();

  const counterState = useSelector((state: RootStoreType) => state.value);

  const handleInputValueChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setError(false);
    setInputValue(+event.target.value);
  };

  const validate = counterState < 500 && counterState > -500;
  const multiplyValueValidation =
    counterState * inputValue < 500 && counterState * inputValue > -500;
  const divideValueValidation =
    counterState / inputValue < 500 && counterState / inputValue > -500;

  const handleCounterIncrement = useCallback(() => {
    if (validate) {
      dispatch(counterIncrementAction());
    }
  }, [dispatch, counterState]);

  const handleCounterDecrement = useCallback(() => {
    if (validate) {
      dispatch(counterDecrementAction());
    }
  }, [dispatch, counterState]);

  const handleCounterDouble = useCallback(() => {
    if (validate) {
      dispatch(counterDoubleAction());
    }
  }, [dispatch, counterState]);

  const handleCounterAddValue = useCallback(() => {
    if (validate) {
      dispatch(counterAddValueAction(inputValue));
    }
  }, [dispatch, inputValue, counterState]);

  const handleCounterRemoveValue = useCallback(() => {
    if (validate) {
      dispatch(counterRemoveValueAction(inputValue));
    }
  }, [dispatch, inputValue, counterState]);

  const handleCounterMultiplyByValue = useCallback(() => {
    if (multiplyValueValidation) {
      dispatch(counterMultiplyByValueAction(inputValue));
    }
  }, [dispatch, inputValue, counterState, multiplyValueValidation]);

  const handleCounterDivideByValue = useCallback(() => {
    if (inputValue === 0) {
      setError(true);
    }
    if (divideValueValidation) {
      dispatch(counterDivideByValueAction(inputValue));
    }
  }, [dispatch, inputValue, counterState]);

  const handleReset = useCallback(() => {
    setError(false);
    dispatch(counterResetAction());
  }, [dispatch]);

  return (
    <div className="App">
      Counter: {counterState}
      <input
        onChange={handleInputValueChange}
        type="number"
        placeholder="Enter number"
      />
      <div>
        <div>
          <button onClick={handleReset}>RESET</button>
        </div>
        <button onClick={handleCounterIncrement}>+1</button>
        <button onClick={handleCounterDecrement}>-1</button>
        <button onClick={handleCounterDouble}>x2</button>
        <div>
          <button onClick={handleCounterAddValue}>+value</button>
          <button onClick={handleCounterRemoveValue}>-value</button>
        </div>
        <div>
          <button onClick={handleCounterMultiplyByValue}>Multiply</button>
          <button onClick={handleCounterDivideByValue}>Divide</button>
          {error && <p>Don't divide by 0!</p>}
        </div>
      </div>
    </div>
  );
}

export default App;
