import { stat } from "fs";
import { ICounterAddValueAction } from "../actions/action-counter-addValue";
import { ICounterDecrementAction } from "../actions/action-counter-decrement";
import { ICounterDivideByValueAction } from "../actions/action-counter-divideByValue";
import { ICounterDoubleAction } from "../actions/action-counter-double";
import { ICounterIncrementAction } from "../actions/action-counter-increment";
import { ICounterMultiplyByValueAction } from "../actions/action-counter-multiplyByValue";
import { ICounterRemoveValueAction } from "../actions/action-counter-removeValue";
import { ICounterResetAction } from "../actions/action-counter-reset";
import { CounterActionTypesEnum } from "../actions/action-types.enum";

export interface ICounterState {
  value: number;
}

const initialState: ICounterState = {
  value: 0,
};

type ICounterActions =
  | ICounterIncrementAction
  | ICounterDecrementAction
  | ICounterDoubleAction
  | ICounterAddValueAction
  | ICounterRemoveValueAction
  | ICounterMultiplyByValueAction
  | ICounterDivideByValueAction
  | ICounterResetAction;

export const counterReducers = (
  state: ICounterState = initialState,
  action: ICounterActions
): ICounterState => {
  switch (action.type) {
    case CounterActionTypesEnum.INCREMENT:
      return { value: state.value + 1 };
    case CounterActionTypesEnum.DECREMENT:
      return { value: state.value - 1 };
    case CounterActionTypesEnum.DOUBLE:
      return { value: state.value * 2 };
    case CounterActionTypesEnum.ADD_VALUE:
      return { value: state.value + action.payload };
    case CounterActionTypesEnum.REMOVE_VALUE:
      return { value: state.value - action.payload };
    case CounterActionTypesEnum.MULTIPLY_BY_VALUE:
      return { value: state.value * action.payload };
    case CounterActionTypesEnum.DIVIDE_BY_VALUE:
      return { value: +(state.value / action.payload).toFixed(2) };
    case CounterActionTypesEnum.RESET:
      return { value: 0 };
    default:
      return state;
  }
};
