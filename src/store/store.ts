import { legacy_createStore as createStore } from "redux";
import { counterReducers } from "../reducers/counterReducer";

export const store = createStore(counterReducers);

export type RootStoreType = ReturnType<typeof store.getState>;
